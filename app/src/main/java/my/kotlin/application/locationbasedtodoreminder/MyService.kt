package my.kotlin.application.locationbasedtodoreminder

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*


/**Service는 메인 스레드에서 기본적으로 동작.
 * 부하가 큰 작업일 때는 별도 스레드 생성 후 작업하는게 좋음.
 * 종료 여부는 온전히 사용자 몫.*/

/**IntentService는 별도 스레드에서 동작.
 * 받은 intent를 순차 실행, 완료하면 알아서 종료됨.
 */

class MyService : Service() {

    private val ANDROID_CHANNEL_ID = "my.kotlin.application.locationbasedtodoreminder"
    private val NOTIFICATION_ID = 9999

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback

    private var mServiceHandler: Handler? = null
    private var mNotificationManager: NotificationManager? = null

    private val TAG: String = "우진"

    private val PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_BACKGROUND_LOCATION
    )

    private val REQUEST_CODE = 999

    override fun onCreate() {

        val handlerThread = HandlerThread(TAG)
        handlerThread.start()
        mServiceHandler = Handler(handlerThread.looper)
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //오레오 버전 이상은 포그라운드 서비스(+고정 알림 포함) 설정을 해주어야 함.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            //Notification(상단 알림) 채널 생성
            val chan = NotificationChannel(
                ANDROID_CHANNEL_ID,
                "MyService",
                NotificationManager.IMPORTANCE_NONE
            )

            chan.lightColor = Color.BLUE
            chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

            //Notification 객체를 가져옴
            mNotificationManager!!.createNotificationChannel(chan)

            //Notification 알림 객체 생성
            val builder = Notification.Builder(this, ANDROID_CHANNEL_ID)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("SmartTracker")

            val notification = builder.build()

            //Notification 알림과 함께 포그라운드 서비스 시작
            startForeground(NOTIFICATION_ID, notification)

            Log.e("우진", "서비스 실행 in onCreate()")
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationRequest = LocationRequest.create()
        locationRequest.run {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//            interval = 5 * 1000
            fastestInterval = 1 * 1000
            smallestDisplacement = 1.toFloat() //기본: 50미터
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                locationResult ?: return

                for (location in locationResult.locations) {
                    Log.e(
                        "우진",
                        "[업데이트된 위치 by FLC on SERVICE] 위도: ${location.latitude} 경도: ${location.longitude}"
                    )
                }
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.e("우진", "서비스 실행 in onStartCommand()")

        val startedFromNotification = intent!!.getBooleanExtra(
            "temp",
            false
        )

        // We got here because the user decided to remove location updates from the notification.

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates()
            stopSelf()
        }

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.e("우진,", "권한 문제 !!!!!!")
        } else {
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
        }

       return START_NOT_STICKY;
    }

    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
        stopForeground(true);
    }

    override fun onRebind(intent: Intent?) {
        super.onRebind(intent)
        stopForeground(true);
    }

    override fun onDestroy() {
        Log.e("우진", "서비스 destroy")
        super.onDestroy()
    }

    fun removeLocationUpdates() {
        Log.i(TAG, "Removing location updates")
        try {
            fusedLocationClient.removeLocationUpdates(locationCallback)
            Utils.setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            Utils.setRequestingLocationUpdates(this, true)
            Log.e(
                TAG,
                "Lost location permission. Could not remove updates. $unlikely"
            )
        }
    }

}