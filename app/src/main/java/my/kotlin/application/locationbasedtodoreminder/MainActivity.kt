package my.kotlin.application.locationbasedtodoreminder

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import my.kotlin.application.locationbasedtodoreminder.Utils.getLocationText
import my.kotlin.application.locationbasedtodoreminder.Utils.requestingLocationUpdates


class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

//    val REQUEST_CODE = 999
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        Log.e("우진", "MainActivity onCreate()")
//
//        val permissionAccessCoarseLocationApproved = ActivityCompat
//            .checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
//                PackageManager.PERMISSION_GRANTED
//
//        if (permissionAccessCoarseLocationApproved) {
//
//            Log.e("우진", "COARSE_LOCATION 허용 확인")
//
//            val backgroundLocationPermissionApproved = ActivityCompat
//                .checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) ==
//                    PackageManager.PERMISSION_GRANTED
//
//            if (backgroundLocationPermissionApproved) {
//
//                Log.e("우진", "BACKGROUND_LOCATION 허용 확인")
//
//                //포그라운드 및 백그라운드에서 위치 엑세스 가능
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    //오레오 이상은 백그라운드로 실행하면 강제 종료 위험 있음 -> 포그라운드 실행해야
//                    startForegroundService(Intent(applicationContext, MyService::class.java))
//                    Log.e("우진", "API 레벨 26 이상")
//                } else {
//                    //백그라운드 실행에 제약 없음
//                    startService(Intent(applicationContext, MyService::class.java))
//                    Log.e("우진", "API 레벨 25 이하")
//                }
//            } else {
//
//                Log.e("우진", "COARSE_LOCATION만 있음")
//
//                //포그라운드에서만 위치 엑세스 가능한 상태
//                ActivityCompat.requestPermissions(
//                    this,
//                    arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
//                    REQUEST_CODE
//                )
//            }
//        } else {
//            Log.e("우진", "권한 둘다 없음")
//
//            //아무 권한이 없는 상태
//            ActivityCompat.requestPermissions(
//                this,
//                arrayOf(
//                    Manifest.permission.ACCESS_COARSE_LOCATION,
//                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
//                ),
//                REQUEST_CODE
//            )
//        }
//
//        button.setOnClickListener {
//            stopService(Intent(applicationContext, MyService::class.java))
//        }
//    }

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private var myReceiver: MyReceiver? = null

    // A reference to the service used to get location updates.
    private var myService: LocationUpdatingService? = null

    // Tracks the bound state of the service.
    private var myBound = false

    // Monitors the state of the connection to the service.
    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(
            name: ComponentName,
            service: IBinder
        ) {
            Log.e("우진", "onServiceConnected()")

            val binder =
                service as LocationUpdatingService.LocalBinder
            myService = binder.service
            myBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Log.e("우진", "onServiceDisconnected()")

            myService = null
            myBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        Log.e("우진", "onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        myReceiver = MyReceiver()

        // Check that the user hasn't revoked permissions by going to Settings.
        if (requestingLocationUpdates(this)) {
            Log.e("우진", "requestingLocationUpdates(this)")
            if (!checkPermissions()) {
                Log.e("우진", "권한 체크 안되어 있으면")
                requestPermissions()
            } else {
                Log.e("우진", "권한 체크 되어 있으면")
            }
        } else {
            Log.e("우진", "false!!!!1")
        }
    }

    override fun onStart() {
        super.onStart()
        PreferenceManager.getDefaultSharedPreferences(this)
            .registerOnSharedPreferenceChangeListener(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //오레오 이상은 백그라운드로 실행하면 강제 종료 위험 있음 -> 포그라운드 실행해야
            startForegroundService(Intent(this, LocationUpdatingService::class.java))
            Log.e("우진", "API 레벨 26 이상")
        } else {
            //백그라운드 실행에 제약 없음
            startService(Intent(this, LocationUpdatingService::class.java))
            Log.e("우진", "API 레벨 25 이하")
        }

        request_location_updates_button.setOnClickListener(View.OnClickListener {
            if (!checkPermissions()) {
                requestPermissions()
            } else {
                // Bind to the service. If the service is in foreground mode, this signals to the service
                // that since this activity is in the foreground, the service can exit foreground mode.
                bindService(
                    Intent(this, LocationUpdatingService::class.java), serviceConnection,
                    Context.BIND_AUTO_CREATE
                )
            }
        })

        remove_location_updates_button.setOnClickListener(View.OnClickListener {
            if (myService != null) {
                myService!!.removeLocationUpdates()
            } else {
                Log.e("우진", "실행중인 서비스가 없습니다")
            }
        })

//        // Restore the state of the buttons when the activity (re)launches.
//        setButtonsState(
//            requestingLocationUpdates(
//                this
//            )
//        )
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(
            myReceiver!!,
            IntentFilter(LocationUpdatingService.ACTION_BROADCAST)
        )
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver!!)
        super.onPause()
    }

    override fun onStop() {
        if (myBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(serviceConnection)
            myBound = false
        }
        PreferenceManager.getDefaultSharedPreferences(this)
            .unregisterOnSharedPreferenceChangeListener(this)

        super.onStop()
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        return PackageManager.PERMISSION_GRANTED ==
                (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ))
    }

    private fun requestPermissions() {
        val shouldProvideRationale =
            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.e(
                TAG,
                "Displaying permission rationale to provide additional context."
            )
            Snackbar.make(
                main_layout,
                "Location permission is needed for core functionality",
                Snackbar.LENGTH_INDEFINITE
            )
                .setAction(R.string.ok, View.OnClickListener { // Request permission
                    ActivityCompat.requestPermissions(
                        this@MainActivity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_PERMISSIONS_REQUEST_CODE
                    )
                })
                .show()
        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(
                    TAG,
                    "User interaction was cancelled."
                )
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                if(myService != null){
                    myService!!.requestLocationUpdates()
                }
            } else {
                // Permission denied.
                setButtonsState(false)
                Snackbar.make(
                    main_layout,
                    "Permission was denied, but is needed for core\n" +
                            "        functionality.",
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(
                        "Settings",
                        View.OnClickListener { // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri: Uri = Uri.fromParts(
                                "package",
                                BuildConfig.APPLICATION_ID, null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        })
                    .show()
            }
        }
    }

    /**
     * Receiver for broadcasts sent by [LocationUpdatingService].
     */
    private inner class MyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val location =
                intent.getParcelableExtra<Location>(LocationUpdatingService.EXTRA_LOCATION)
            if (location != null) {
                Toast.makeText(
                    this@MainActivity,
                    getLocationText(
                        location
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onSharedPreferenceChanged(
        sharedPreferences: SharedPreferences,
        s: String
    ) {
        // Update the buttons state depending on whether location updates are being requested.
        if (s == Utils.KEY_REQUESTING_LOCATION_UPDATES) {
            setButtonsState(
                sharedPreferences.getBoolean(
                    Utils.KEY_REQUESTING_LOCATION_UPDATES,
                    false
                )
            )
        }
    }

    private fun setButtonsState(requestingLocationUpdates: Boolean) {
        if (requestingLocationUpdates) {
            request_location_updates_button.setEnabled(false)
            remove_location_updates_button.setEnabled(true)
        } else {
            request_location_updates_button.setEnabled(true)
            remove_location_updates_button.setEnabled(false)
        }
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName

        // Used in checking for runtime permissions.
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }
}
