package my.kotlin.application.locationbasedtodoreminder

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.location.Location
import android.os.*
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.*
import my.kotlin.application.locationbasedtodoreminder.Utils.requestingLocationUpdates
import my.kotlin.application.locationbasedtodoreminder.Utils.setRequestingLocationUpdates


class LocationUpdatingService : Service() {

    private val mBinder: IBinder = LocalBinder()

    private var mChangingConfiguration = false

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationCallback: LocationCallback? = null

    private lateinit var locationRequest: LocationRequest

    private var currentLocation: Location? = null

    private var serviceHandler: Handler? = null

    private var notificationManager: NotificationManager? = null

    private var changingConfiguration = false

    override fun onCreate() {

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationCallback = object : LocationCallback() {

            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                onNewLocation(locationResult.lastLocation)

                Log.e("우진", "locationCallback is called")
            }
        }

        createLocationRequest()
        requestLocationUpdates()

//        getLastLocation()

        val handlerThread = HandlerThread("우진")
        handlerThread.start()
        serviceHandler = Handler(handlerThread.looper)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = getString(R.string.app_name)
            // Create the channel for the notification
            val channel = NotificationChannel(
                LocationUpdatingService.CHANNEL_ID,
                name,
                NotificationManager.IMPORTANCE_DEFAULT
            )

            // Set the Notification Channel for the Notification Manager.
            notificationManager!!.createNotificationChannel(channel)

            startForeground(LocationUpdatingService.NOTIFICATION_ID, notification)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        Log.e("우진", "기본 Service 실행")

        val startedFromNotification = intent!!.getBooleanExtra(
            LocationUpdatingService.EXTRA_STARTED_FROM_NOTIFICATION,
            false
        )

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates()
            stopSelf()
        }
        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        changingConfiguration = true
    }

    override fun onBind(intent: Intent?): IBinder? {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.e("우진", "바인드 service 실행")
        stopForeground(true)
        mChangingConfiguration = false
        return mBinder
    }

    override fun onRebind(intent: Intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.e("우진", "in onRebind()")
        stopForeground(true)
        mChangingConfiguration = false
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent): Boolean {
        Log.e("우진", "Last client unbound from service")

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && Utils.requestingLocationUpdates(this)) {
            Log.e("우진", "Starting foreground service")
            startForeground(LocationUpdatingService.NOTIFICATION_ID, notification)
        }
        return true // Ensures onRebind() is called when a client re-binds.
    }

    override fun onDestroy() {
        serviceHandler!!.removeCallbacksAndMessages(null)
    }

    fun createLocationRequest() {
        locationRequest = LocationRequest.create()
        locationRequest.run {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//            interval = 5 * 1000
            fastestInterval = 1 * 1000
            smallestDisplacement = 1.toFloat() //기본: 50미터
        }
    }

    fun requestLocationUpdates() {
        Log.e("우진", "위치 업데이트 요청!")

        setRequestingLocationUpdates(
            this,
            true
        )

        try {
            fusedLocationClient!!.requestLocationUpdates(
                locationRequest,
                locationCallback, Looper.myLooper()
            )
        } catch (unlikely: SecurityException) {
            setRequestingLocationUpdates(
                this,
                false
            )
            Log.e(
                "우진",
                "위치 업데이트 요청 실패 -> $unlikely"
            )
        }
    }

    fun removeLocationUpdates() {
        Log.e("우진", "Removing location updates")
        try {
            fusedLocationClient!!.removeLocationUpdates(locationCallback)
            Utils.setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            Utils.setRequestingLocationUpdates(this, true)
            Log.e(
                "우진",
                "Lost location permission. Could not remove updates. $unlikely"
            )
        }
    }

    private val notification: Notification
        private get() {
            val intent = Intent(this, LocationUpdatingService::class.java)
            val text: CharSequence = Utils.getLocationText(currentLocation)

            // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
            intent.putExtra(LocationUpdatingService.EXTRA_STARTED_FROM_NOTIFICATION, true)

            // The PendingIntent that leads to a call to onStartCommand() in this service.
            val servicePendingIntent = PendingIntent.getService(
                this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

            // The PendingIntent to launch activity.
            val activityPendingIntent = PendingIntent.getActivity(
                this, 0,
                Intent(this, MainActivity::class.java), 0
            )

            val builder = NotificationCompat.Builder(this)
                .addAction(
                    R.drawable.ic_launch, getString(R.string.launch_activity),
                    activityPendingIntent
                )
                .addAction(
                    R.drawable.ic_cancel, getString(R.string.remove_location_updates),
                    servicePendingIntent
                )
                .setContentText(text)
                .setContentTitle(Utils.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(text)
                .setWhen(System.currentTimeMillis())

            // Set the Channel ID for Android O.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(LocationUpdatingService.CHANNEL_ID) // Channel ID
            }
            return builder.build()
        }

    private fun onNewLocation(location: Location) {

        Log.e("우진", "Updated Loction: ${location.latitude}, ${location.longitude}")

        currentLocation = location

        // Notify anyone listening for broadcasts about the new location.
        val intent = Intent(ACTION_BROADCAST)
        intent.putExtra(EXTRA_LOCATION, location)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)

        // Update notification content if running as a foreground service.
        if (serviceIsRunningInForeground(this)) {
            notificationManager!!.notify(
                NOTIFICATION_ID,
                notification
            )
        }
    }

    private fun getLastLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        else{
            fusedLocationClient!!.lastLocation
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        currentLocation = task.result
                        Log.e(
                            "우진",
                            "Last location: ${currentLocation}"
                        )
                    } else {
                        Log.e(
                            "우진",
                            "Failed to get location."
                        )
                    }
                }
        }
    }

//    private val lastLocation: Unit
//        private get() {
//            try {
//                fusedLocationClient!!.lastLocation
//                    .addOnCompleteListener { task -> if (task.isSuccessful && task.result != null) {
//                        currentLocation = task.result
//                    } else {
//                        Log.e(
//                            "우진",
//                            "Failed to get location."
//                        )
//                    }
//                    }
//            } catch (unlikely: SecurityException) {
//                Log.e(
//                    "우진",
//                    "Lost location permission.$unlikely"
//                )
//            }
//        }

    fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(
            Context.ACTIVITY_SERVICE
        ) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (javaClass.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }

    inner class LocalBinder : Binder() {
        val service: LocationUpdatingService
            get() = this@LocationUpdatingService
    }

    companion object {

        private const val PACKAGE_NAME =
            "my.kotlin.application.locationbasedtodoreminder\n"

        const val CHANNEL_ID = "channel_01"
        const val ACTION_BROADCAST =
            "$PACKAGE_NAME.broadcast"
        const val EXTRA_LOCATION =
            "$PACKAGE_NAME.location"
        private const val EXTRA_STARTED_FROM_NOTIFICATION =
            PACKAGE_NAME +
                    ".started_from_notification"

        const val NOTIFICATION_ID = 12345678
    }
}

